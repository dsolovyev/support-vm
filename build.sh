#!/bin/sh

set -ex

mkdir -p "$2"
prepackage_dir="$(cd "$2" && pwd)"
cd "$1"

./configure \
  --prefix=/usr/local \
  --with-nogui \
  \
  --enable-cdrom \
  --enable-pci \
  --enable-a20-pin \
  \
  --disable-smp \
  --enable-cpu-level=6 \
  --enable-x86-64 \
  --enable-fpu \
  --disable-protection-keys \
  --disable-avx \
  --disable-evex \
  --disable-vmx \
  --disable-svm \
  \
  --disable-debugger \
  --disable-debugger-gui \
  --disable-disasm \
  \
  --enable-repeat-speedups \
  --disable-plugins \
  --disable-clgd54xx \
  --disable-voodoo \
  --disable-sb16 \
  --disable-es1370 \
  --enable-ne2000 \
  --disable-pnic \
  --disable-e1000

cpus=`grep -c ^processor /proc/cpuinfo` || cpus=1
make -j $cpus

make DESTDIR="$prepackage_dir" install
